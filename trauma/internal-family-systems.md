# Internal Family Systems

The Internal Family Systems Model (IFS) is a form of individual therapy focused on thinking about the mind as being made up of many parts, each of which can be examined individually, and how those parts work together. It was created by [Dr. Richard Schwartz](https://ifs-institute.com/about-us/richard-c-schwartz-phd).

It can be very useful for plural systems, since it looks at *everyone* as being made up of a set of interrelating parts. 

See more about it on [Wikipedia](https://en.wikipedia.org/wiki/Internal_Family_Systems_Model), the [IFS Institute](https://ifs-institute.com), and [Psychology Today](https://www.psychologytoday.com/us/therapy-types/internal-family-systems-therapy).

## Books

### No Bad Parts - Healing Trauma and Restoring Wholeness with the Internal Family Systems Model

By [Dr. Richard Schwartz](https://ifs-institute.com/about-us/richard-c-schwartz-phd).

A useful guide to Internal Family Systems therapy, walking the reader through exercises to get to know their inner selves. While not *just* helpful to plural systems or those who think they may be plural, many of the techniques and concepts that the book explains can be used effectively between headmates.

Find out more at the IFS Institute [here](https://ifs-institute.com/nobadparts).
