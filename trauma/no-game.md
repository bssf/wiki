# The No Game

This is a game introduced to the discord by fi, used to help deal with both rejection sensitive dysphoria and a fear of refusing people's requests.

## Steps

1. Find a partner you trust. One of you will be the Asker, the other the Refuser
2. Agree that for the next 15 minutes, the Asker will make arbitrary requests, and the Refuser will, without exception, say no.
    - The Asker's requests should start absurd and only get moreso, and most importantly, they should be for things *the Asker absolutely does not want*
    - In fi's example, she starts off with "60 pounds of charbroiled dodo steaks", and only gets more absurd from there.

This gets the Asker's brain used to being refused in a situation with no consequences, and therefore used to the idea that being rejected is not the end of the world. Similarly, it gets the Refuser's brain used to saying no to the requests of people they care about, and used to the idea that it will not hurt people to be denied sometimes.
