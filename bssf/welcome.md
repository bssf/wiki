# So I Just Joined The Server, What Should I Know?

Welcome to Blue Shell Strike Force! We're a lovely (If I do say so myself) group of (mostly) queer (mostly) nerds, generally brought together by thinking [0xabad1dea](https://infosec.exchange/@0xabad1dea) is pretty neat! We hope you'll have a lovely time here.

## What Are All These Channels About?

We don't have as many channels as many Discords, but we *do* tend to have long-running threads, which can be hard to find. So here's a list of our channels and some important threads to help you find them!

### FLUFF AREA
This is for the more light-hearted talk.

* #chat - general chat, talk about whatever you want :3
    * Blue Shell Anime Watch Party - a thread for weekly anime watch parties organised by @quantumjump. We currently watch at 7pm UTC on Sundays. More details in the pinned message!
    * Cuteposting
    * foodshare
    * random music
    * Who wants train photos
* #trans-gressions - Lots of queer talk goes on here! It was originally a thread in #chat, but got promoted to its own channel. It should be noted that you do **NOT** have to be trans to participate! Just that there are often trans-related conversations going on in there. A few of us also do daily tarot readings there for some reason?
    * the trans wiki - This is the thread where we make this website! :D I don't know why it's in the trans channel
* #code - for talking about programming!
* #gamersonly - video game chat!
    * ashlands waiting room (aka Valheim) - Valheim
    * Duolingo thread - this is a game, right??
    * FFXIV - Moon's Haunted - *racks pistol* Moon's haunted
    * Prime Satisfactors - Satisfactory thread
* #minecraft - This has a separate channel to #gamersonly because it actually came first!

### TRAUMA ZONE
The channels here are where heavier stuff gets sequestered. Think of these channels as kind of a "containment zone". Some of them are mutual support channels, some are more "some people don't want to see this stuff so it gets put in its own area".

* #quarantine - general purpose badfeels channel. If you're having trouble with something and you don't know where to turn, this is your best bet. Be aware, others here may be dealing with brainworms of their own.
* #trans-ponders - the trans-flavoured version of #quarantine. Where #quarantine spawned from #chat, #trans-ponders spawned from #trans-gressions. The same warnings apply here.
* #robotrevolution - for talking about, sharing the output of, and shitting on AI stuff.
* #socialmediapocalypse - bemoaning the death of twitter, the state of the fediverse or bsky, etc.

## Server Dialect
There are a few turns of phrase which are peculiar to our corner of the internet, so if you're confused about what react someone just gave your message, these might help.

* :herb: emoji - this indicates general good vibes. "Blessings be on this post".
* cw fi ||spoilers|| - this is something that our resident witch fi[nominal] does with some of her messages (and others of us have picked up the habit). It serves as a warning that the spoilered text may change the way you think of the world. Usually in a good way, but change should only be affected on people with their consent.

## Reading List
There are a few books we tend to recommend to a lot of people. If you're here, you'll likely find these books helpful, enjoyable, or both.

* The entire Discworld Series by Terry Pratchett. - It's a bit daunting, but there are plenty of people who will be able to help you choose where to start :)
* What To Say Next: Successful Communication in Work, Life and Love with ASD - An incredible book for autistic and other neurodivergent people on how to successfully communicate with neurotypicals and also other nd people.
* Complex PTSD: From Surviving to Thriving: A Guide and Map for Recovering from Childhood Trauma
* The Body Keeps the Score: Brain, Mind and Body in the Healing of Trauma
* No Bad Parts - A description and workbook for the [Internal Family Systems](../trauma/internal-family-systems.md) therapy system, by its creator.
* Unmasking Autism: Discovering the New Faces of Neurodiversity
