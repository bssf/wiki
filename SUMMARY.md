# Blue Shell Strike Force Wiki

[About](README.md)
[Unsorted Links](links)
[I Just Joined, What Now?](bssf/welcome.md)

<!-- NOTE: Keep this sorted alphabetically -->

- [Neurospicy](neurospicy/README.md)
  - [Brili](neurospicy/brili.md)
- [Plurality](plurality/README.md)
- [Relationships](relationships/README.md)
- [Self Care](self-care/README.md)
  - [Skincare](self-care/skincare.md)
- [Sexuality](sexuality/README.md)
- [Transfemme](transfemme/README.md)
  - [Clothes](transfemme/clothes.md)
- [Trauma](trauma/README.md)
  - [Internal Family Systems](trauma/internal-family-systems.md)
  - [The No Game](trauma/no-game.md)
