# [Blue Shell Strike Force Wiki](https://bssf.gitlab.io/wiki/)

This is a markdown wiki for the Blue Shell Strike Force community, with a trans, queer, and neurospicy focus. You can find a rendered copy at [https://bssf.gitlab.io/wiki/](https://bssf.gitlab.io/wiki/).

If you're not part of the BSSF community and still want to coordinate, please reach out to a maintainer!

## Topic Index

- [I Just Joined The Server, What Now?](./bssf/welcome)
- [Neurospiciness](./neurospicy)
    - [Brili](./neurospicy/brili)
- [Plurality](./plurality)
- [Relationships](./relationships)
- [Self Care](./self-care)
    - [Skincare](./self-care/skincare)
- [Sexuality](./sexuality)
- [Transfemme](./transfemme)
- [Trauma](./trauma)
    - [The No Game](./trauma/no-game)

For consistency, use lowercase file names with hyphens (`-`) to separate words. Everything in a file or directory name will show up in a URL. As an example, *Skincare* is at `./self-care/skincare.md`.

When adding a new directory or significant file, add a link here, without any trailing slash or `.md` suffix. As an example, [Skincare](./self-care/skincare)'s link looks like this: `[Skincare](./self-care/skincare)`

## Contributing

This is a wiki. If you have edit access, you have edit access. Use it, but be nice!

- Notify the thread before making any edits: <https://discord.com/channels/687724694174040103/1179261709135908904>
- Discuss more-extensive changes with others, and write drafts in branches.
  - Merge requests are optional. Use your best judgement. You're allowed to commit to main, but be wise about it.
- Avoid image uploads. The repository has limited storage space.
- Strip URLs of tracking information, and do not use affiliate links.

### What do I agree to when contributing?

We won't make you sign a CLA ([example](https://www.apache.org/licenses/contributor-agreements.html)), but the following legalese's still necessary.

When contributing to this wiki, you agree to:
- Release your contributions under the terms of the [**Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International**](./LICENSE) license ([rendered](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)).
- Certify your contributions according to the terms of the [**Developer Certificate of Origin**](./DCO) ([source](https://developercertificate.org))

<!-- TODO: Give a clear textual explanation of the above. -->

### How do I get access?

- Join the Blue Shell Strike Force community.
- Create a GitLab account, if you don't already have one.
- Request access to the BSSF GitLab group.
- Ping a maintainer with your GitLab username: <https://discord.com/channels/687724694174040103/1179261709135908904>

#### [Maintainers] How do I grant access?

Unfortunately, you need to have the **Owner** role for the following. This is a GitLab limitation.

- If responding to an **access request** (Preferred):
  - Open https://gitlab.com/groups/bssf/-/group_members?tab=access_requests
  - Verify that:
    - The requested role is **Developer**
    - The user is on the Discord
- If adding a user manually (Not Preferred):
  - Open https://gitlab.com/groups/bssf/-/group_members
  - Add GitLab usernames in the text input field if necessary
  - Select the **Developer** role in the dropdown
  - Click the **Invite** button

### How do I edit a page?

Before starting an edit, post in the thread. This ensures that there's no duplication of effort.

From the repository:
- Go to the repository's home page: https://gitlab.com/bssf/wiki
- Click on the file you wish to edit.
- Click the blue **Edit** dropdown menu.
- Click **Edit single file**.
  - Alternatively, if you like the Web IDE, use that instead.
  - Make sure that you have the "add end of file new line" setting active.

From the rendered wiki:
- Click on the edit icon in the top right.
  - It should look like this: <i class="fa fa-edit" ></i>

## Maintainers

- @dr-syn (Secondary Group Owner)
- @kouhaidev (Primary Group Owner) - `@kouhai@social.treehouse.systems` (Mastodon)
- @randomnetcat
- @casuallyblue
- @demize (Secondary Group Owner)
