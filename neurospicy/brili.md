# Brili

[Brili](https://brili.com) is an app developed by Brili GmbH, designed to help organise and focus on routines for ADHD people. It was originally developed by parents of ADHD children to help them concentrate on daily routines such as getting ready for school, but was so popular among ADHD adults that they made a version aimed at adult users.

Both versions of the app (family & adult) are subscription based.

It works by allowing you to set up routines ahead of time, listing everything you need to do on an average day to, say, get ready for work in the mornings (brush hair, brush teeth, get dressed, have breakfast, take meds etc.) You order these in the routine, and give each item a goal time (e.g. brush teeth: 3min). You can set a routine either to begin at a certain time, or to end at a certain time, or to be an "on-demand" routine which is manually triggered.

When you run a routine, your phone shows a card with whatever the next item on the list is, along with how long left you have in your estimated time for that task. You can swipe left to mark an item as done, at which point the next item in the list will pop up. Alternatively you can swipe right to postpone the item one place in the list, down to reorder the list for this run of the routine, or up to say "I'm not doing this item this time".

Each item can also have notes set which will show up when that item is currently active, so you can for instance have "clean shower" as an item, with notes on that item going through the steps of cleaning the shower.

## Testimonial

(this section is my (QuantumJump's) personal thoughts on the app).

The upshot of all this is that you can do all the order-of-operations and subtask breakdown work ONCE, under no time pressure, and then whenever you run the routine the only thing you have to think about is "what is the next thing my phone is telling me to do". It removes a vast amount of cognitive load from the actual performing of these routines.

It reduced the time it takes me from getting up in the morning to being ready for work from 1h 30m to a mere 30-45m, a 50-66% reduction in time, and a massive reduction in "oh god did I forget anything??" anxiety.

## Differences between the Adult and Kids versions
I personally use the adult version of brili, because the family version has a weird data model if you're using it for yourself where you need to be your own "parent", whereas the adult version doesn't have that problem. However, the family version does have a couple of features which I miss in the adult version. Specifically:
* The kids version's sounds are set to be media sounds rather than notification sounds on android, which means that they play even when your phone is on silent. I find this super useful.
* The kids version will average how much time you take for specific tasks over time, so you can adjust how much time you allocate to do them in future.
